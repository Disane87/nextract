# @disane/nextract

**Just a random tool to exctract text by regex out of specific files** ✨

This is my first npm package to extract text by a regex out of a folder and genererates different output formats like 

# Usage

``` 
    ===================================================
     ______   ___   _______  _______  __    _  _______
    |      | |   | |       ||   _   ||  |  | ||       |
    |  _    ||   | |  _____||  |_|  ||   |_| ||    ___|
    | | |   ||   | | |_____ |       ||       ||   |___
    | |_|   ||   | |_____  ||       ||  _    ||    ___|
    |       ||   |  _____| ||   _   || | |   ||   |___
    |______| |___| |_______||__| |__||_|  |__||_______|

    // Package: @disane/nextract
    Tool to extract text out of specific filetypes by regex

    // HP:      https://gitlab.com/Disane87/nextract/#readme
    ===================================================

Usage: 
    nextract <command> [options]

Example:
    nextract search -f -p  "C:/temp" 

Commands:
    search              Searches files by regex in all file types in path

Options:
  --version             Show version number                            [boolean]
  --help                Show help                                      [boolean]
  -p, --path            Path to search in                               [string]
  -s, --searchrequests  Patterns to search in path                       [array]
  -f, --fullpath        Outputs the full path of found files
                                                      [boolean] [default: false]
  -o, --output          Outputs the full path of found files
                          [choices: "console", "json", "html", "file"] [default: "file"]
```

## Installing
`npm install @disane/nextract -g ` or locally
`npm install @disane/nextract --save-dev` 

## Using it via cli
```
nextract search -f -p  "C:/temp" -o "console"
```
Output:

```
Output mode: console
SearchPath:  C:/temp
Searching for RegEx '(?<=\{\{).*?(?=\ *\|\ *translate)' in '.html'. Please wait.
\test.html
⟹       [1:2] TEST (string)
```

Content of test file:
```html
<div>{{ 'TEST' | translate }}</div>
```
### Output as html file (default browser will be opened)
```
nextract search -f -p  "C:/temp" -o "html"
```
![html overview](https://gitlab.com/Disane87/nextract/raw/dev/docs/html_overview.PNG)


## Using it as a module in typescript/node

### With output as file
```js
const nextract = require('@disane/nextract');

// import * as nextract from '@disane/nextract';
nextract.main({p: "C:/temp", o: 'file'});

test = nextract.getResultsFromFile();
console.log(test);
```

### With output as json
```js
extract.main({p: "C:/temp"}).then((results)=> {
    console.log(results);
});
```

## With custom search patterns
```js

const searchRequests = [
    { searchPattern: '(flying)', fileType: '.txt'}
];

// import * from '@disane/translation-extract';
extract.main({p: "c:/temp", s: searchRequests}).then((results)=> {
    console.log(results);
});

```

### Output as html file (default browser will be opened)
```js
const extract = require('@disane/nextract');

const searchRequests = [
    { searchPattern: '(flying)', fileType: '.txt'}
];

// import * from '@disane/translation-extract';
extract.main({p: "c:/temp", s: searchRequests, o: 'html'});
```
![html overview](https://gitlab.com/Disane87/nextract/raw/dev/docs/html_overview.PNG)


## General output format
Results are always returned as an array of Type `SearchRequests`. For every search request, there will be a returned object of the following type:

```ts
export interface SearchRequests {
    searchPattern: string;
    fileType: string;
    results?: FoundFiles[];
    folder?: string;
}
export interface FoundFiles {
    fileName: string;
    occurances: Position[];
}

export interface Position {
    lineNumber: number;
    rowNumber: number;
    line?: string;
    translations?: Translation[];
}
export interface Translation {
    language?: string;
    placeholder: string;
    translation?: string;
    placeholderType: 'code' | 'string';
}

```

# License

MIT © Marco Franke