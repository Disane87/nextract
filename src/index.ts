#!/usr/bin/env node
'use strict';
/**
 * @module nextract
 * @desc NodeJS text extract. Extracts text by regex and file types and summerizes it as html, console logs or json
 */

const chalk = require('chalk');
const path = require('path');

import { getFile, getResultsFromFile, initCommands, saveResults, searchStringInFileTypes, servePage, startServer } from './common/';
import { FoundFiles, OutputMode, SearchRequests, StartMode } from './common/interfaces';

export { getResultsFromFile };

let startMode = StartMode.cli;

if (require.main === module) {
    startMode = StartMode.cli;
    main(initCommands());
} else {
    startMode = StartMode.module;
}

console.log(`Startmode: ${StartMode[startMode]}`);

/**
 * Main function
 *
 * @export
 * @param {*} args
 * @returns {Promise<SearchRequests[]>}
 */
export function main(args: any): Promise<SearchRequests[]> {

    const argv = require('yargs').argv;

    let searchPath = args.path || args.p || argv.p || argv.path;
    const fullpath = args.fullpath || args.f || argv.fullpath || argv.f;
    let output = OutputMode[args.output || args.o || argv.output || argv.o]; // || 'json';
    let searchrequests =  args.searchrequests || args.s || argv.searchrequests || argv.s;

    if (!startMode) {
        startMode = StartMode.module;
    }

    if (startMode === StartMode.module) {
        if (!output) {
            output = OutputMode[OutputMode.json];
        }
    }
    console.log(`Output mode: ${OutputMode[output]}`);

    if (!searchPath) {
        const config = JSON.parse(getFile('config.json'));
        // console.log(`Searching '${text}' in all '${fileType}'`);
        if (config) {
            // console.log('Got config: ', JSON.stringify(config));
        }
        searchPath = config.searchPath;
    }

    console.log('SearchPath: ', searchPath);
    let searchRequests: SearchRequests[] = [];
    if(!searchrequests){
        searchRequests = [
            { searchPattern: '(?<=\\{\\{).*?(?=\\ *\\|\\ *translate)', fileType: '.html'},
            { searchPattern: '(?<=\\.instant\\(\\ *).*?(?=\\ *\\))', fileType: '.ts'},
        ];
    } else {
        searchRequests = searchrequests;
    }

    let fileCount = 0;
    searchRequests.forEach((searchRequest) => {
        const searchPattern = (searchRequest.searchPattern); // .replace('\\', '\\\\');
        console.log(`Searching for RegEx '${searchPattern}' in '${searchRequest.fileType}'. Please wait. `);
        const foundFiles: FoundFiles[] = searchStringInFileTypes(searchRequest.fileType, searchPath, searchPattern);

        if (output === OutputMode.console) {
            foundFiles.forEach((f) => {
                const file = f;
                const fileName = (fullpath ? file.fileName : path.normalize(file.fileName).replace(path.normalize(searchPath.trim()), ''));
                console.log(`${chalk.green(fileName)}`);
                file.occurances.forEach((o) => {
                    let placeholder = '';
                    if (o.translations) {
                        o.translations.forEach((t) => {
                            if (t.placeholder !== '') {
                                placeholder += `${t.placeholder} (${chalk.blue(t.placeholderType)}), `;
                            } else {
                                placeholder += `${chalk.red('Kein Platzhalter ermittelt!')}, `;
                            }
                        });
                    }
                    placeholder = placeholder.trim().slice(0, -1);
                    console.log(`⟹\t[${o.lineNumber}:${o.rowNumber}] ${placeholder}`); // \n${o.line}
                });
                fileCount += 1;
            });
        }

        searchRequest.folder = path.normalize(searchPath.trim());

        if (output === OutputMode.json || output === OutputMode.html) {
            foundFiles.forEach((f) => {
                f.fileName = (fullpath ? f.fileName : path.normalize(f.fileName).replace(path.normalize(searchPath.trim()), ''));
            });

            searchRequest.results = foundFiles;
        }

    });
    if (output === OutputMode.console) {
        console.log(`Found files: ${fileCount}`);
    }

    if (output === OutputMode.json) {
        console.log(searchRequests);
        return new Promise<SearchRequests[]>((resolve, reject) => {
            resolve(searchRequests);
        });
    }

    if (output === OutputMode.html) {
        startServer(1187);
        servePage('/', searchRequests);
    }

    if (output === OutputMode.file) {
        saveResults(searchRequests);
        // servePage('/', searchRequests);
    }
}
