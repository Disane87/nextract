const path = require('path');
const fs = require('fs');

import { isUpperCase } from './helper';
import { FoundFiles, Position, SearchRequests, Translation } from './interfaces';

export function saveResults(results: SearchRequests[]) {
    fs.writeFile('results.json', JSON.stringify(results), 'utf8', (err) => {
        if (err) {
            return console.log(err);
        }
        console.log('The file was saved!');
    });
}

export function  getResultsFromFile(): SearchRequests[] {
    return JSON.parse(fs.readFileSync('results.json'));
}

export function searchStringInFileTypes(fileType, filePath: string, searchPattern: string): FoundFiles[] {
    const searchPath = filePath;

    const files = searchFilesInDirectory(searchPath, fileType, searchPattern);
    if (files) {
        return files;
    }
    return [];
}

export function getFile(filePath: string): string {
    let content = '';
    const cwd = process.cwd();

    filePath = path.join(cwd, filePath);

    // console.log('Getting file: ', filePath);

    if (fs.existsSync(filePath)) {
        content = fs.readFileSync(filePath, 'utf8');
    } else {
        const data = JSON.stringify({ searchPath: ''});
        fs.writeFileSync(filePath, data);
    }

    return content;
}

function searchFilesInDirectory(dir, ext, searchPattern): FoundFiles[] {

    dir = dir.trim();

    if (!fs.existsSync(dir)) {
        console.log(`Specified directory: ${dir} does not exist`);
        return;
    }

    // const files = fs.readdirSync(dir);
    const found = getFilesInDirectory(dir, ext);
    const foundFiles = [];

    found.forEach((file) => {
        const fileContent = fs.readFileSync(file);

        // We want full words, so we use full word boundary in regex.
        const regex = new RegExp(searchPattern, 'g');
        if (regex.test(fileContent)) {
            // console.log(`Your word was found in file: ${file.replace(dir.replace("/", "\\"), "")}`);

            const positions: Position[] = findMatchesInLines(fileContent.toString(), searchPattern);

            const f: FoundFiles = {
                fileName: path.normalize(file).replace(dir.replace('/', '\\'), ''),
                occurances: positions,
            };
            foundFiles.push(f);
        }
    });
    return foundFiles;
}

function findMatchesInLines(text, searchPattern): Position[] {
    const endOfLine = require('os').EOL;
    const matchingLines: number[] = [];
    const matchingRows: number[] = [];
    const allLines = text.split(endOfLine);
    const positions: Position[] = [];

    for (let i = 0; i < allLines.length; i++) {
        const match = allLines[i].match(new RegExp(searchPattern, 'g'));

        if (match) {
            matchingLines.push(i);

            const rowMatchIndex = allLines[i].trim().replace('\t', '').indexOf(match[0]);
            matchingRows.push(rowMatchIndex);
        }
    }

    matchingLines.forEach((l, index) => {
        const i = l;
        const line = allLines[i];
        if (line) {
            const rowNum = matchingRows[index];
            const p = new RegExp(searchPattern, 'g');
            const placeHolderMatches = line.trim().match(p);

            const pos: Position = {
                lineNumber: l + 1,
                line: allLines[i].trim(),
                rowNumber: rowNum
            };

            if (placeHolderMatches) {
                const translationsFound: Translation[] = [];
                placeHolderMatches.forEach((phm) => {
                    const placeholderString = phm.trim().replace(/\'/g, '');

                    translationsFound.push({
                        placeholder: placeholderString,
                        placeholderType: (isUpperCase(placeholderString) ? 'string' : 'code')
                    });
                });
                pos.translations = translationsFound;
            }
            positions.push(pos);
        }

    });

    return positions;

    // return {
    //     lineNumber: matchingLines,
    //     rowNumber: matchingRows,
    //     translations: [],

    // };
}

// Using recursion, we find every file with the desired extention, even if its deeply nested in subfolders.
function getFilesInDirectory(dir, ext) {
    if (!fs.existsSync(dir)) {
        // console.log(`Specified directory: ${dir} does not exist`);
        return;
    }

    let files = [];
    fs.readdirSync(dir).forEach((file) => {
        const filePath = path.join(dir, file);
        const stat = fs.lstatSync(filePath);

        // If we hit a directory, apply our function to that dir. If we hit a file, add it to the array of files.
        if (stat.isDirectory()) {
            const nestedFiles = getFilesInDirectory(filePath, ext);

            files = files.concat(nestedFiles);
        } else {
            if (path.extname(file) === ext) {
                files.push(filePath);
            }
        }
    });

    return files;
}
