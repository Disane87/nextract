export function isUpperCase(c) {
    c = c.replace(/\W/g, '');
    if (c.match('.*[a-z].*')) {
        return false;
    }

    return true;
}

/**
 * https://stackoverflow.com/questions/3115150/how-to-escape-regular-expression-special-characters-using-javascript
 *
 * @param {any} text
 * @returns {string} escaped string of regex
 */
export function escapeRegExp(text): string {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}
