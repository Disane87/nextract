import { watermark } from './watermark';
const yargsObj = require('yargs');

export function initCommands(): any {
    const search = {
        command: 'search',
        desc: 'Searches for patterns in specific filetypes in a chosen path',
        builder: (yargs) => yargs
         .options({
            p: {
                alias: 'path',
                demandOption: false,
                describe: 'Path to search in',
                type: 'string',
            },
            s: {
                alias: 'searchrequests',
                demandOption: false,
                describe: 'Patterns to search in path',
                type: 'array',
            },
            f: {
                alias: 'fullpath',
                demandOption: false,
                default: false,
                describe: 'Outputs the full path of found files',
                type: 'boolean',
            },
            o: {
                alias: 'output',
                demandOption: false,
                default: 'file',
                describe: 'Outputs the full path of found files',
                type: 'choices',
                choices: ['console', 'json', 'html', 'file']
            },
            fp: {
                alias: 'filepath',
                demandOption: false,
                describe: 'Outputs the full path of found files',
                type: 'choices',
                choices: ['console', 'json', 'html']
            }
        })
        .usage(`${watermark()}\nUsage: $0 <command> [options]`)
        // handler: (argv) => {
        // //   if (!argv._handled) { console.log('search handler:', argv); }
        //   argv._handled = true;
        //   main(argv);
        // },
      };

    // tslint:disable-next-line:no-unused-expression
    yargsObj
    .usage(`${watermark()}\nUsage: $0 <command> [options]`)
    .command(search)
    .demandCommand(1, 'You need to set the search path.')
    .showHelpOnFail(true, 'Specify --help for available options')
    .locale('en')
    .help('help');

    return yargsObj;
}
