export interface SearchRequests {
    searchPattern: string;
    fileType: string;
    results?: FoundFiles[];
    folder?: string;
}
export interface FoundFiles {
    fileName: string;
    occurances: Position[];
}

export interface Position {
    lineNumber: number;
    rowNumber: number;
    line?: string;
    translations?: Translation[];
}

export interface Translation {
    language?: string;
    placeholder: string;
    translation?: string;
    placeholderType: 'code' | 'string';
}

export enum StartMode {
    'cli',
    'module'
}

export enum OutputMode {
    'console' = 'console',
    'html' = 'html',
    'json' = 'json',
    'file' = 'file'
}
