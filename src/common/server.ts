const http = require('http');
const fs = require('fs');
const express = require('express');
const path = require('path');
const opn = require('opn');
let server: any = {};
const app = express();

export function startServer(port) {

    app.set('view engine', 'pug');
    app.set('views', path.join(__dirname, '../assets'));

    server = app.listen(port, () => {
        const url = `http://127.0.0.1:${port}`;
        console.log(`Server ist opened. Navgate to: ${url}`);
        opn(url);
    });

    process.on('SIGINT', () => {
        stopServer();
    });

    process.on('SIGTERM', () => {
        stopServer();
    });

}

export function servePage(endpoint: string = '/', data: object) {
    app.get(endpoint, (req, res) => {
        // const htmlData = ;
        res.render('index', { title: 'Search results', dataObj: data });
    });

}

export function stopServer() {
    server.close(() => {
        console.info('Server closed');
        process.exit(0);
    });
}
